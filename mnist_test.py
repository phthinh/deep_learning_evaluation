import numpy as np
import tensorflow as tf
import pickle
from tensorflow.keras import Model

mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

# Add a channels dimension
x_train = x_train[..., tf.newaxis]
x_test = x_test[..., tf.newaxis]

class NeuralNetworkModel(Model):
    def __init__(self):
        super(NeuralNetworkModel, self).__init__()
        self.flatten = tf.keras.layers.Flatten()
        self.dense1 = tf.keras.layers.Dense(128, activation='relu')
        self.dense2 = tf.keras.layers.Dense(64, activation='relu')
        self.dense3 = tf.keras.layers.Dense(32, activation='relu')
        self.dense4 = tf.keras.layers.Dense(16, activation='relu')
        self.dense5 = tf.keras.layers.Dense(10, activation='softmax')

    @tf.function
    def call(self, x):
        x = self.flatten(x)
        x = self.dense1(x)
        x = self.dense2(x)
        x = self.dense3(x)
        x = self.dense4(x)
        x = self.dense5(x)
        return x

# parameters
BATCH_SIZE = 1000
BUFFER_SIZE = 20000
epoch = 100
# end parameters

# process data
train_ds = tf.data.Dataset.from_tensor_slices(
    (x_train, y_train)).shuffle(10000).batch(32)

test_ds = tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(32)

# training
model = NeuralNetworkModel()
loss = tf.keras.losses.SparseCategoricalCrossentropy()
optimizer = tf.keras.optimizers.Adam(learning_rate = 0.0001)
train_loss = tf.keras.metrics.Mean()
test_loss = tf.keras.metrics.Mean()

train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy()
test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy()


def train_step(data, label):
    with tf.GradientTape() as tape:
        prediction = model(data)
        loss_object = loss(y_true=label, y_pred=prediction)
    gradients = tape.gradient(loss_object, model.trainable_variables)
    optimizer.apply_gradients(zip(gradients, model.trainable_variables))

    train_loss(loss_object)
    train_accuracy(y_true=label, y_pred=prediction)


def test_step(data, label):
    prediction = model(data)
    loss_object = loss(y_true=label, y_pred=prediction)

    test_loss(loss_object)
    test_accuracy(y_true=label, y_pred=prediction)

for i in range(epoch):
    for n, (data, label) in enumerate(train_ds):
        train_step(data, label)
        if (n+1) % 10 == 0:
            print("Training: Epoch {}|{} Loss {} Accuracy {}".format(i+1, epoch, train_loss.result(), train_accuracy.result() * 100))

    for n, (data, label) in enumerate(test_ds):
        test_step(data, label)
    print("Testing: Epoch {}|{} Loss {} Accuracy {}".format(i+1, epoch, test_loss.result(), test_accuracy.result() * 100))

    test_loss.reset_states()
    test_accuracy.reset_states()
    train_loss.reset_states()
    train_accuracy.reset_states()






