import numpy as np
import tensorflow as tf
import pickle
from tensorflow.keras import Model
import math
from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()

def process_data():
    data = np.load('data_set_taken', allow_pickle = True)
    train, test = data[0][0:3], data[1][0:3]
    # each train, test is a list of 11 elements, each has shape (9000,100) (train), (1000,100) (test)
    train_data = np.concatenate(train, axis=0)
    train_data = (train_data - np.mean(train_data, axis=0)) / (1e-6 + np.std(train_data, axis=0))
    train_label = [[i] * 9000 for i in range(3)] # create labels
    train_label = [item for sublist in train_label for item in sublist] # flattern train_label
    train_label = np.array(train_label, dtype=np.int32)

    test_data = np.concatenate(test, axis=0)
    test_data = (test_data - np.mean(test_data, axis=0)) / (1e-6 + np.std(test_data, axis=0))
    test_label = [[i] * 1000 for i in range(3)]  # create labels
    test_label = [item for sublist in test_label for item in sublist]  # flattern train_label
    test_label = np.array(test_label, dtype=np.int32)

    return train_data, train_label, test_data, test_label

def process_data_time_series():
    data = np.load('data_set_taken', allow_pickle = True)
    train, test = data[0][0:3], data[1][0:3]
    # each train, test is a list of 11 elements, each has shape (9000,100) (train), (1000,100) (test)
    train_data = np.concatenate(train, axis=0).reshape(-1, 100, 1)
    train_data = (train_data - np.mean(train_data, axis=0)) / (1e-6 + np.std(train_data, axis=0))
    train_label = [[i] * 9000 for i in range(3)] # create labels
    train_label = [item for sublist in train_label for item in sublist] # flattern train_label
    train_label = np.array(train_label, dtype=np.int32)

    test_data = np.concatenate(test, axis=0).reshape(-1, 100, 1)
    test_data = (test_data - np.mean(test_data, axis=0)) / (1e-6 + np.std(test_data, axis=0))
    test_label = [[i] * 1000 for i in range(3)]  # create labels
    test_label = [item for sublist in test_label for item in sublist]  # flattern train_label
    test_label = np.array(test_label, dtype=np.int32)

    return train_data, tf.one_hot(train_label, 3), test_data, tf.one_hot(test_label, 3)

def process_data_time_series_fft():
    data = np.load('data_set_taken', allow_pickle = True)
    train, test = data[0][0:6:2], data[1][0:6:2]
    # each train, test is a list of 11 elements, each has shape (9000,100) (train), (1000,100) (test)
    train_data = np.concatenate(train, axis=0).reshape(-1, 100)
    train_data_fft = np.fft.fft(train_data, axis=1, norm='ortho')
    train_data_real = train_data_fft.real
    train_data_imag = train_data_fft.imag
    train_data = np.stack([train_data_real, train_data_imag], axis=2)
    train_data = (train_data - np.mean(train_data, axis=0)) / (1e-6 + np.std(train_data, axis=0))

    train_label = [[i] * 9000 for i in range(3)] # create labels
    train_label = [item for sublist in train_label for item in sublist] # flattern train_label
    train_label = np.array(train_label, dtype=np.int32)

    test_data = np.concatenate(test, axis=0).reshape(-1, 100)
    test_data_fft = np.fft.fft(test_data, axis=1, norm='ortho')
    test_data_real = test_data_fft.real
    test_data_imag = test_data_fft.imag
    test_data = np.stack([test_data_real, test_data_imag], axis=2)
    test_data = (test_data - np.mean(test_data, axis=0)) / (1e-6 + np.std(test_data, axis=0))

    test_label = [[i] * 1000 for i in range(3)]  # create labels
    test_label = [item for sublist in test_label for item in sublist]  # flattern train_label
    test_label = np.array(test_label, dtype=np.int32)

    return train_data, tf.one_hot(train_label, 3), test_data, tf.one_hot(test_label, 3)

class NeuralNetworkModel(Model):
    def __init__(self):
        super(NeuralNetworkModel, self).__init__()
        self.dense1 = tf.keras.layers.Dense(1024, activation='relu')
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.dense2 = tf.keras.layers.Dense(512, activation='relu')
        self.dense3 = tf.keras.layers.Dense(256, activation='relu')
        self.dense4 = tf.keras.layers.Dense(64, activation='relu')
        self.dense5 = tf.keras.layers.Dense(3, activation='softmax')

    @tf.function
    def call(self, x):
        x = self.dense1(x)
        x = self.bn1(x)
        x = self.dense2(x)
        x = self.dense3(x)
        x = self.dense4(x)
        x = self.dense5(x)
        return x

class SequentialModel_LSTM(Model):
    def __init__(self):
        super(SequentialModel_LSTM, self).__init__()
        self.lstm1 = tf.keras.layers.LSTM(512, return_sequences=True, activation=self.gelu_activation)
        self.lstm2 = tf.keras.layers.LSTM(32, activation=self.gelu_activation)
        self.ga = tf.keras.layers.GlobalAveragePooling1D()
        self.dense1 = tf.keras.layers.Dense(32, activation=self.gelu_activation)
        self.dense2 = tf.keras.layers.Dense(16, activation=self.gelu_activation)
        self.dense3 = tf.keras.layers.Dense(3)

    def gelu_activation(self, x):
        return 0.5 * x * (1 + tf.tanh(tf.sqrt(2 / math.pi) * (x + 0.044715 * tf.pow(x, 3))))

    def call(self, x, isTraining = True):
        x = self.lstm1(x)
        x = self.lstm2(x)
        #x = self.ga(x)
        x = self.dense1(x)
        x = self.dense2(x)
        return self.dense3(x)

# parameters
BATCH_SIZE = 200
BUFFER_SIZE = 20000
epoch = 100
# end parameters

# process data
#train_data, train_label, test_data, test_label = process_data()
#train_data, train_label, test_data, test_label = process_data_time_series()
train_data, train_label, test_data, test_label = process_data_time_series_fft()

train_data = tf.data.Dataset.from_tensor_slices((train_data, train_label))
train_data = train_data.shuffle(BUFFER_SIZE).batch(BATCH_SIZE)

test_data = tf.data.Dataset.from_tensor_slices((test_data, test_label))
test_data = test_data.shuffle(BUFFER_SIZE).batch(BATCH_SIZE)

# training
#model = NeuralNetworkModel()
model = SequentialModel_LSTM()
loss_object = tf.keras.losses.CategoricalCrossentropy()
optimizer = tf.keras.optimizers.Adam(learning_rate = 0.001)
train_loss = tf.keras.metrics.Mean()
test_loss = tf.keras.metrics.Mean()

train_accuracy = tf.keras.metrics.CategoricalAccuracy()
test_accuracy = tf.keras.metrics.CategoricalAccuracy()


def train_step(data, label):
    with tf.GradientTape() as tape:
        prediction = model(data)
        #print(prediction)
        loss = loss_object(y_true=label, y_pred=prediction)
    gradients = tape.gradient(loss, model.trainable_variables)
    optimizer.apply_gradients(zip(gradients, model.trainable_variables))

    train_loss(loss)
    train_accuracy(y_true=label, y_pred=prediction)


def test_step(data, label):
    prediction = model(data)
    loss = loss_object(y_true=label, y_pred=prediction)

    test_loss(loss)
    test_accuracy(y_true=label, y_pred=prediction)

for i in range(epoch):
    for n, (data, label) in enumerate(train_data):
        train_step(data, label)
        #if (n+1) % (BATCH_SIZE * 2) == 0:
        print("Training: Epoch {}|{} Iteration {}|{} Loss {} Accuracy {}".format(i+1, epoch,  (n+1)*BATCH_SIZE, 99000, train_loss.result(), train_accuracy.result() * 100))

    for n, (data, label) in enumerate(test_data):
        test_step(data, label)
    print("Testing: Epoch {}|{} Loss {} Accuracy {}".format(i+1, epoch, test_loss.result(), test_accuracy.result() * 100))

    test_loss.reset_states()
    test_accuracy.reset_states()
    train_loss.reset_states()
    train_accuracy.reset_states()






